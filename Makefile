# Include standard NID (NSO in Docker) package Makefile that defines all
# standard make targets
include nidpackage.mk

# The following are specific to this repositories packages
testenv-start-extra:

testenv-test:
	$(MAKE) testenv-test-clean
	$(MAKE) testenv-test-pua-basic
	$(MAKE) testenv-test-clean
	$(MAKE) testenv-test-pacing-wait
	$(MAKE) testenv-test-clean
	$(MAKE) testenv-test-pacing-factor
	$(MAKE) testenv-test-clean
	$(MAKE) testenv-test-emergency-stop

testenv-test-clean:
	$(MAKE) testenv-runcmdJ CMD="configure\n delete pua-tester\n commit"
	$(MAKE) testenv-runcmdJ CMD="configure\n delete post-upgrade-actions\n commit"

testenv-test-pua-basic:
	@echo "\n== Verify basic PUA functionality"
	$(MAKE) testenv-runcmdJ CMD="configure\n set pua-tester foo0\n set pua-tester foo1\n set pua-tester foo2\n set pua-tester foo3\n set pua-tester foo4\n set pua-tester foo5\n set pua-tester foo6\n set pua-tester foo7\n set pua-tester foo8\n set pua-tester foo9\n commit"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="configure\n set post-upgrade-actions post-upgrade-action foo-factor xpath /pua-tester:pua-tester action-name increment enabled\n commit"
	$(MAKE) testenv-runcmdJ CMD="request packages reload"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	sleep 40
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^10$$"
	$(MAKE) testenv-runcmdJ CMD="show post-upgrade-actions | tab"

testenv-test-pacing-wait:
	@echo "\n== Verify that the wait pacing method works"
	$(MAKE) testenv-runcmdJ CMD="configure\n set pua-tester foo1\n set pua-tester foo2\n set pua-tester foo3\n set pua-tester foo4\n commit"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="configure\n set post-upgrade-actions post-upgrade-action foo-wait xpath /pua-tester:pua-tester action-name increment enabled pacing wait 10\n commit"
	$(MAKE) testenv-runcmdJ CMD="request packages reload"
	@echo "-- PUA worker sleeps for 30 seconds at start. Then it should immediately"
	@echo "   run one PUA, wait for the configured 10 seconds and run a second. We"
	@echo "   give it two seconds of margin for a total of 42 seconds"
	sleep 42
	@echo "-- After which we should see 2 instances completed"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^2$$"
	sleep 21
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^4$$"
	$(MAKE) testenv-runcmdJ CMD="show post-upgrade-actions | tab"

testenv-test-pacing-factor:
	@echo "\n== Verify that the factor pacing method works"
	$(MAKE) testenv-runcmdJ CMD="configure\n set pua-tester foo1 sleep-duration 5\n set pua-tester foo2 sleep-duration 5\n set pua-tester foo3 sleep-duration 5\n set pua-tester foo4 sleep-duration 5\n commit"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="configure\n set post-upgrade-actions post-upgrade-action foo xpath /pua-tester:pua-tester action-name increment enabled\n commit"
	$(MAKE) testenv-runcmdJ CMD="request packages reload"
	@echo "-- PUA worker sleeps for 30 seconds at start. Then it should immediately"
	@echo "   run one PUA, which in this case takes ~5 seconds, the factor pacing"
	@echo "   method then waits another 5 seconds, runs the next action which will"
	@echo "   take another 5 seconds. We give it a second of margin for a total of"
	@echo "   46 seconds"
	sleep 46
	@echo "-- After which we should see 2 instances completed"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^2$$"
	sleep 21
	@echo "-- And finally all instances completed"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^4$$"
	$(MAKE) testenv-runcmdJ CMD="show post-upgrade-actions | tab"

testenv-test-emergency-stop:
	@echo "\n== Verify that the emergency-stop action works"
	$(MAKE) testenv-runcmdJ CMD="configure\n set pua-tester foo1 sleep-duration 5\n set pua-tester foo2 sleep-duration 5\n commit"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="configure\n set post-upgrade-actions post-upgrade-action foo xpath /pua-tester:pua-tester action-name increment enabled\n commit"
	$(MAKE) testenv-runcmdJ CMD="request packages reload"
	@echo "-- PUA worker sleeps for 30 seconds at start. Then it should immediately"
	@echo "   run one PUA, which in this case takes 5 seconds. Before the action is"
	@echo "   completed, we signal the worker to stop after 1 seconds. We give it a"
	@echo "   second of margin for a total of 32 seconds"
	sleep 32
	@echo "-- Signal the worker to stop immediately"
	$(MAKE) testenv-runcmdJ CMD="request post-upgrade-actions emergency-stop"
	@echo "-- The first action is still running. We wait for additional 3 seconds for the"
	@echo "   action to complete, then another 5 seconds for the pacing period. We give"
	@echo "   it a second of margin for a total of 9 seconds."
	@echo "   Note: the first action will complete execution, but the progress will not"
	@echo "   be marked in PUA list."
	sleep 9
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^1$$"
	@echo "-- The PUA worker has been disabled. The second action should not have"
	@echo "   been executed. Sleep for 5+1 seconds and verify."
	sleep 6
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 1338 | wc -l | grep "^1$$"
	@echo "-- Enable the worker and wait for all actions to execute. Due to the"
	@echo "   stochastic nature of the worker, the first action may be executed first."
	@echo "   We must wait 30 seconds for the worker to start, then 2x5 seconds for the"
	@echo "   two actions and 5 seconds of pacing between actions. With a margin of 1 second"
	@echo "   the total is 46 seconds."
	$(MAKE) testenv-runcmdJ CMD="configure\n set post-upgrade-actions enabled\n commit"
	sleep 46
	@echo "-- And finally all instances completed - actions were executed at least once."
	$(MAKE) testenv-runcmdJ CMD="show pua-tester"
	$(MAKE) testenv-runcmdJ CMD="show pua-tester" | grep 133[89] | wc -l | grep "^2$$"
	$(MAKE) testenv-runcmdJ CMD="show post-upgrade-actions | tab"
