import datetime

import ncs


def is_ha_master_or_no_ha():
    with ncs.maapi.single_read_trans("", "system", db=ncs.OPERATIONAL) as trans:
        if trans.exists("/tfnm:ncs-state/tfnm:ha"):
            mode = str(ncs.maagic.get_node(trans,
                                           '/tfnm:ncs-state/tfnm:ha/tfnm:mode'))
            return (mode == 'master')
        else:
            return True


def format_yang_date_and_time(timestamp: datetime.datetime=None, microseconds=False) -> str:
    """Format a timestamp in yang:date-and-time format

    :param timestamp: optional datetime.datetime instance. Defaults to utcnow()
    :return: yang:date-and-time formatted string"""
    if not timestamp:
        timestamp = datetime.datetime.utcnow()
    elif not isinstance(timestamp, datetime.datetime):
        raise ValueError('timestamp must be datetime.datetime instance')
    if microseconds:
        fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
    else:
        fmt = '%Y-%m-%dT%H:%M:%SZ'
    return timestamp.strftime(fmt)
