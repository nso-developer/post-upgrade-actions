import glob
import logging
import os
import time
import timeit
from functools import partial
from typing import List, Set

import _ncs
import ncs
from bgworker import background_process
from . import utils


def __find_actions(t_read: ncs.maapi.Transaction, action_name: str,
                   results: Set[str], errors: List[str], log,
                   kp: str, value_unused):
    action_kp = '{}/{}'.format(kp, action_name)
    try:
        action = ncs.maagic.get_node(t_read, action_kp)
    except Exception as e:
        message = 'error finding action {}: {}'.format(action_kp, e)
        errors.append(message)
        log.error(message)
    else:
        if not isinstance(action, ncs.maagic.Action):
            message = '{} is not an action'.format(action_kp)
            errors.append(message)
            log.warning(message)
        else:
            log.debug('found action {}'.format(action_kp))
            results.add(action_kp)


def __refresh_actions(pua_path: str, actions: Set[str], errors: List[str]) -> Set[str]:
    """Refresh the list of post-upgrade action for a task

    :param pua_path: keypath to the post-upgrade-actions list entry
    :param actions: set of action keypaths that match the post-upgrade xpath
    :param errors: a list of error messages, occurred while we were finding actions
    :return: a set of completed action keypaths

    First, remove actions that no longer exist, then create entries for new actions.
    """
    completed_actions = set()
    with ncs.maapi.single_write_trans('python-post-upgrade-oper-write', 'system',
                                      db=ncs.OPERATIONAL) as t_write:
        pua_write = ncs.maagic.get_node(t_write, pua_path)
        removed_actions = [action.path for action in pua_write.action if action.path not in actions]
        map(pua_write.action.delete, removed_actions)
        for action_kp in actions:
            action = pua_write.action.create(action_kp)
            if action.completed:
                completed_actions.add(action_kp)

        if errors:
            pua_write.error = '\n'.join(errors)
        t_write.apply()
    return completed_actions


def _merge_configs(log):
    if ncs.LIB_VSN_STR >= '06060000':
        # NCS >= 4.6 has MAAPI flags defined in ncs.maapi
        load_flags = ncs.maapi.CONFIG_MERGE | ncs.maapi.CONFIG_XML      # pylint: disable=no-member
    else:
        # older versions have MAAPI flags in _ncs.maapi
        load_flags = _ncs.maapi.CONFIG_MERGE | _ncs.maapi.CONFIG_XML    # pylint: disable=no-member
    config_dir = os.path.join(os.path.dirname(__file__), '..', '..', 'post-upgrade-actions')
    for config in glob.glob(os.path.join(config_dir, '*.xml')):
        log.debug('loading {}'.format(config))
        try:
            with ncs.maapi.single_write_trans('python-post-upgrade-write', 'system') as t_write:
                t_write.load_config(flags=load_flags, filename=config)
                t_write.apply()
        except Exception as e:
            log.error('error loading {}: {}'.format(config, e))


def post_upgrade(wait=30):
    """Entry point into the background worker process

    Before attempting to execute configured post-upgrade actions, wait for a bit to let the
    callpoints register. If HA is enabled, this worker will only run on the master node (guaranteed
    by the background_process module).
    """
    log = logging.getLogger('post-upgrade')
    log.info('post-upgrade-actions worker initialized')
    while True:
        time.sleep(wait)
        post_upgrade_cycle(log)


def post_upgrade_cycle(log):
    """Find non-processed or failed post-upgrade actions and execute them."""
    with ncs.maapi.single_read_trans('python-post-upgrade-read', 'system') as t_read:
        root = ncs.maagic.get_root(t_read)
        for pua in root.post_upgrade_actions.post_upgrade_action:
            if pua.completed:
                continue

            if pua.enabled is False:
                log.debug('action {} disabled'.format(pua.name))
                continue
            log.debug('processing action {}, xpath={}'.format(pua.name, pua.xpath))
            actions: Set[str] = set()
            find_action_errors: List[str] = []
            t_read.xpath_eval(pua.xpath,
                              partial(__find_actions, t_read, pua.action_name, actions, find_action_errors, log),
                              trace=None, path='')

            completed_actions = __refresh_actions(pua._path, actions, find_action_errors)

            # execute the actions!
            wait = 0
            for action_kp in actions:
                success = True
                message = None
                log.debug('sleeping for {}s until next action {}'.format(wait, action_kp))
                time.sleep(wait)
                start = timeit.default_timer()
                try:
                    log.debug('executing action {}'.format(action_kp))
                    with ncs.maapi.single_read_trans('PUA-read-{}'.format(action_kp), 'system') as action_read:
                        action = ncs.maagic.get_node(action_read, action_kp)
                        result = action()
                    # Our action outputs usually have a (success, message) result.
                    # NCS built-in actions may have (result, info)
                    if hasattr(result, 'success'):
                        success = result.success
                    elif hasattr(result, 'result'):
                        success = result.result
                    if hasattr(result, 'message'):
                        message = result.message
                    elif hasattr(result, 'info'):
                        message = result.info
                    if success:
                        completed_actions.add(action_kp)
                except Exception as e:
                    success = False
                    message = str(e)
                end = timeit.default_timer()

                if str(pua.pacing.pacing_method) == 'factor':
                    wait = (end-start) * pua.pacing.factor
                else:
                    wait = pua.pacing.wait

                log.info('action {} result {}'.format(action_kp, success or message))
                with ncs.maapi.single_write_trans('python-post-upgrade-oper-write', 'system', db=ncs.OPERATIONAL) as t_write:
                    pua_write = ncs.maagic.get_node(t_write, pua._path)
                    pua_write.action[action_kp].duration = int((end-start) * 100)
                    if success:
                        pua_write.action[action_kp].completed = True
                        pua_write.action[action_kp].completed_at = utils.format_yang_date_and_time()
                        del pua_write.action[action_kp].error
                    else:
                        pua_write.action[action_kp].error = message or 'Unknown error'
                    t_write.apply()
            if len(completed_actions) == len(actions) and not find_action_errors:
                log.debug('processed all actions')
                with ncs.maapi.single_write_trans('python-post-upgrade-oper-write', 'system', db=ncs.OPERATIONAL) as t_write:
                    pua_write = ncs.maagic.get_node(t_write, pua._path)
                    pua_write.completed = True
                    pua_write.completed_at = utils.format_yang_date_and_time()
                    del pua_write.error
                    t_write.apply()


class PostUpgradeActionEmergencyStop(ncs.dp.Action):
    def init(self, bg_worker):  # pylint: disable=arguments-differ
        self.bg_worker = bg_worker

    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output, t_read):
        self.bg_worker.emergency_stop()
        if self.bg_worker.config_path:
            action_output.result = f'Background worker stopped successfully. To restart, enable by setting {self.bg_worker.config_path}'
        else:
            action_output.result = 'Background worker stopped successfully. To restart, reload the package.'


class PostUpgradeApp(ncs.application.Application):
    worker = None

    def setup(self):
        _merge_configs(self.log)
        self.worker = background_process.Process(self, post_upgrade, config_path='/post-upgrade-actions/enabled')
        self.register_action('post-upgrade-actions-emergency-stop', PostUpgradeActionEmergencyStop, init_args=self.worker)
        self.worker.start()

    def teardown(self):
        self.log.info('{} WorkerApp teardown()'.format(__name__))
        self.worker.stop()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    l = logging.getLogger('post-upgrade')
    _merge_configs(l)
    post_upgrade_cycle(l)
