# Post Upgrade Actions

## Description

Making changes to code in NCS, either in service packages or NEDs, often
requires that the state held in NCS/CDB be updated. NCS already features CDB
upgrade logic for handling certain cases but it doesn't cover everything. For
example, after changing a CLI NED, it is appropriate to run a `sync-from` on all
devices that is using that NED.

The classic approach to running such 'post upgrade actions' is to write detailed
instructions in the release notes and rely on the person performing the upgrade
to carefully read the release notes and carry out the necessary commands.

Instead, this package provides the facilities for running 'post upgrade actions'
(PUAs) in an automated fashion. The definition of the PUAs can be defined at
development time, at the same time that the service logic or NED changes are
happening. This is highly beneficial as it can then also be tested in CI and
becomes part of the artifact shipped into production.

Since PUAs are created at development time and not at run time, they need to be
generic and not instance specific, which is why they are specified in terms of
an XPath and action-name combination. The XPath will be evaluated and could
result in multiple instances which are then executed. This allows each PUA to be
generic and not instance specific. The actions matching the XPath query are
executed one at a time, with a configurable (static / dynamic based on previous
execution) timeout in between.

The PUAs are loaded in from XML files and persisted in CDB. Loading XML files
allows the PUAs to be merged in to the configuration of a production NCS server.
By persisting the PUAs, this package keeps track over time if the PUAs have been
executed or not. This prevents PUAs to be executed multiple times as well as
resuming execution if NCS is restarted. It also means the name of each PUA needs
to be unique over time, so even if you are doing the same thing, like running
`sync-from` on all devices, you need to give the PUA a unique name so that it
can be run again for an NCS instance which has already completed the previous
`sync-from` PUA.


## Usage

To configure a post-upgrade action, provide a name, XPath and action name.

While it is possible to configure the actions through the CLI, the preferred
way when shipping a new version is to include an XML config file. Any valid
configs placed in the `./post-upgrade-actions` package folder will be merged
to CDB config on package load.

```xml
<config xmlns="http://tail-f.com/ns/config/1.0">
  <post-upgrade-actions xmlns="http://terastrm.net/ns/yang/post-upgrade-actions.yang">
    <name>Sync-from on all devices</name>
    <xpath>/devices/device[starts-with(name, 'dut-')]</xpath>
    <action-name>sync-from</action-name>
  </post-upgrade-actions>
</config>
```

_Note: just adding the configuration will not run the post-upgrade actions
immediately. A package reload or NCS restart is still required._

The default pacing mode is _factor=1_, meaning the next action will be executed
after waiting for 1x time it took to execute the previous action. A fixed wait
period may also be used.

After a new post-upgrade action is configured, it will be executed after the
next package reload. Operators can track progress and monitor errors through
operational data:

```
admin@ncs> show post-upgrade-actions
NAME  COMPLETED  COMPLETED AT               ERROR                                                        PATH                                   COMPLETED  COMPLETED AT               DURATION  ERROR
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bob   false      2019-02-06T09:51:13+00:00  error finding action /ncs:devices/device{dut-r1}/bob: 'bob'  /ncs:devices/device{dut-r1}/sync-from  false      2019-02-06T09:51:13+00:00  161       -
```
