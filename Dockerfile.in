ARG NSO_IMAGE_PATH
ARG NSO_VERSION

# DEP_START
# DEP_END


# Compile local packages in the build stage
FROM ${NSO_IMAGE_PATH}cisco-nso-dev:${NSO_VERSION} AS build
ARG PKG_FILE

# DEP_INC_START
# DEP_INC_END

COPY / /src

# TODO: upstream this
# Provide a fake environment for pylint and similar.
#
# All packages are normally assumed to be in the same directory which means that
# if you have a dependency to another directory, the relative path is
# '../dep-pkg'. When the code of a package is included as a dependency, the code
# is required not only at run time but must also be accessible at build time
# such that linters and other static checkers can find all dependencies.
#
# In our build here we keep packages, test-packages and includes separate as
# that greatly simplifies later producing the testnso and package images. In
# order to allow linters to run, we fake the view of a normal directory
# structure and placement of packages (in the same directory) by using symlinks.
# Dependencies will appear as siblings to packages.
RUN for INC in $(find /includes -mindepth 1 -maxdepth 1 -type d); do ln -s ${INC} /src/packages/; ln -s ${INC} /src/test-packages/; done

# Compile packages and inject build-meta-data.xml if it doesn't exist
RUN for PKG in $(find /src/packages /src/test-packages -mindepth 1 -maxdepth 1 -type d); do make -C ${PKG}/src || exit 1; make -f /src/nid/bmd.mk -C ${PKG} build-meta-data.xml; done

# Remove faking symlinks to avoid polluting output
RUN for INC in $(find /includes -mindepth 1 -maxdepth 1 -type d | xargs -n1 basename); do rm -f /src/packages/${INC}; rm -f /src/test-packages/${INC}; done


# produce an NSO image that comes loaded with our NED - perfect for our testing,
# but probably not anything beyond that since you typically want more NSO
# packages for a production environment
FROM ${NSO_IMAGE_PATH}cisco-nso-base:${NSO_VERSION} AS testnso

COPY --from=build /includes/ /var/opt/ncs/packages/
COPY --from=build /src/packages/ /var/opt/ncs/packages/
COPY --from=build /src/test-packages/ /var/opt/ncs/packages/

# Copy in extra files as an overlay, for example additions to
# /etc/ncs/pre-ncs-start.d/
COPY extra-files /


# build a minimal image that only contains the package itself - perfect way to
# distribute the compiled package by relying on Docker package registry
# infrastructure
FROM scratch AS package
COPY --from=build /src/packages/ /var/opt/ncs/packages/
